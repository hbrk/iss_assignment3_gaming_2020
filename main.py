import pygame
import json
with open("./config.json") as config_file:
    data=json.load(config_file)
for column in data:
    success1=data['SUCCESS']['SUCCESS1']
    success2=data['SUCCESS']['SUCCESS2']
    failure1=data['FAILURE']['FAILURE1']
    failure2=data['FAILURE']['FAILURE2']
    failure3=data['FAILURE']['FAILURE3']
    font1=data['FONT']['FONT1']
    speed1=int(data['SPEED']['SPEED1'])
    speed2=int(data['SPEED']['SPEED2'])
    speed3=int(data['SPEED']['SPEED3'])
    speed4=int(data['SPEED']['SPEED4'])
    speed5=int(data['SPEED']['SPEED5'])
    speed6=int(data['SPEED']['SPEED6'])
pygame.init()
X=600
Y=480
score=0
score1=0
win = pygame.display.set_mode((X,Y))
pygame.display.set_caption("Crossy River")
font = pygame.font.Font(font1, 15) 
walkRight = [pygame.image.load('img/R1.png'), pygame.image.load('img/R2.png'), pygame.image.load('img/R3.png'), pygame.image.load('img/R4.png'), pygame.image.load('img/R5.png'), pygame.image.load('img/R6.png'), pygame.image.load('img/R7.png'), pygame.image.load('img/R8.png'), pygame.image.load('img/R9.png')]
walkLeft = [pygame.image.load('img/L1.png'), pygame.image.load('img/L2.png'), pygame.image.load('img/L3.png'), pygame.image.load('img/L4.png'), pygame.image.load('img/L5.png'), pygame.image.load('img/L6.png'), pygame.image.load('img/L7.png'), pygame.image.load('img/L8.png'), pygame.image.load('img/L9.png')]
walkRightE = [pygame.image.load('img/R1E.png'), pygame.image.load('img/R2E.png'), pygame.image.load('img/R3E.png'), pygame.image.load('img/R4E.png'), pygame.image.load('img/R5E.png'), pygame.image.load('img/R6E.png'), pygame.image.load('img/R7E.png'), pygame.image.load('img/R8E.png'),pygame.image.load('img/R9E.png')]
walkLeftE= [pygame.image.load('img/L1E.png'), pygame.image.load('img/L2E.png'), pygame.image.load('img/L3E.png'), pygame.image.load('img/L4E.png'), pygame.image.load('img/L5E.png'), pygame.image.load('img/L6E.png'), pygame.image.load('img/L7E.png'), pygame.image.load('img/L8E.png'),pygame.image.load('img/L9E.png')]
bg = pygame.image.load('img/bg.jpg')
char = pygame.image.load('img/standing.png')
ship=pygame.image.load('img/shipflat.png')
cloud=pygame.image.load('img/cloud.png')
dia=pygame.image.load('img/hill.png')
obs=pygame.image.load('img/bomb.png')
sprite=[pygame.image.load('img/shipflat.png'),pygame.image.load('img/cloud.png'),pygame.image.load('img/hill.png'),pygame.image.load('img/bomb.png')]
clock = pygame.time.Clock()
music = pygame.mixer.music.load('img/music.mp3')
pygame.mixer.music.play(-1)

class player(object): # for the player
    def __init__(self,x,y,width,height):
        self.x=x
        self.y=y
        self.width=width
        self.height=height
        self.vel=5
        self.left=False #use left arrow key for going left for player
        self.right=False #use right arrow key for going right for player
        self.up=False   #use up arrow key for going up for player
        self.down=False #use down arrow key for going down for player
        self.walkCount=0
    def draw(self,win):
        if self.walkCount + 1 >= 27:
                self.walkCount = 0
        if self.left:  
                win.blit(walkLeft[self.walkCount//3], (self.x,self.y))
                self.walkCount += 1                          
        elif self.right:
                win.blit(walkRight[self.walkCount//3], (self.x,self.y))
                self.walkCount += 1
        else:
                win.blit(char, (self.x, self.y))
                self.walkCount = 0
class player1(object): # for the player1
    def __init__(self,x,y,width,height):
        self.x=x
        self.y=y
        self.width=width
        self.height=height
        self.vel=5
        self.l=False    #use l key for going left for player1
        self.r=False    #use r key for going right for player1
        self.u=False    #use u key for going up for player1
        self.d=False    #use d key for going down for player1
        self.walkCount=0
    def draw1(self,win):
        if self.walkCount + 1 >= 27:
                self.walkCount = 0
        if self.l:  
                win.blit(walkLeftE[self.walkCount//3], (self.x,self.y))
                self.walkCount += 1                          
        elif self.r:
                win.blit(walkRightE[self.walkCount//3], (self.x,self.y))
                self.walkCount += 1
        else:
                win.blit(walkLeftE[0], (self.x, self.y))
                self.walkCount = 0
#For the middle paths which are partitioning the window
class path(object):
	def __init__(self,x,y,width,height):
		self.x=x
		self.y=y
		self.width=width
		self.height=height
	def draw_path(self,win):   
		pygame.draw.rect(win,(210,105,30), (self.x, self.y, self.width, self.height))  
#For the first and last paths from up which are also partitioning the window at edges(additionally text is added here)
class tpath(object):
    def __init__(self,x,y,width,height,x1,y1,txt):
        self.x=x
        self.y=y
        self.width=width
        self.height=height
        self.txt=txt
        self.x1=x1
        self.y1=y1
    def draw_tpath(self,win): #paths consisting "START" or "END" text along with obstacles
        pygame.draw.rect(win,(210,105,30), (self.x, self.y, self.width, self.height))
        win.blit(self.txt,(self.x1,self.y1))
    def draw_t1path(self,win):
        win.blit(self.txt,(self.x1,self.y1))
#Fixed obstacles
class spath(object):    
	def __init__(self,x,y):
		self.x=x
		self.y=y
	def draw_spath(self,win):
		win.blit(ship,(self.x,self.y)) #ship 
	def draw_opath(self,win):
		win.blit(dia,(self.x,self.y)) #hill
	def draw_o1path(self,win):
		win.blit(cloud,(self.x,self.y))  #cloud
	def draw_o2path(self,win):
		win.blit(obs,(self.x,self.y))     #bomb
    
#moving obstacles
class move(object):  
    def __init__(self,x,y,vel):
        self.x=x
        self.y=y
        self.vel=vel
    def draw_mpath(self,win):
        win.blit(ship,(self.x,self.y))  #ship in movement
        self.x=self.x+self.vel
        if self.x > 600:
            self.x=0
    def draw_mopath(self,win):
        win.blit(obs,(self.x,self.y))   #bomb in movement
        self.x=self.x+self.vel
        if self.x > 600:
            self.x=0
    def draw_mo1path(self,win):
        win.blit(cloud,(self.x,self.y)) #cloud in movement
        self.x=self.x+self.vel
        if self.x > 600:
            self.x=0
    def draw_mo2path(self,win):
        win.blit(obs,(self.x,self.y)) #bomb in movement
        self.x=self.x+self.vel
        if self.x > 600:
            self.x=500
#Redrawing of game window with some partitions,moved and fixed obstacles(like ship,bomb,cloud,hill)
def redrawGameWindow():
    win.blit(bg, (0,0))
    pathh.draw_t1path(win)
    pathh1.draw_t1path(win)
    path3.draw_tpath(win)
    path4.draw_path(win)
    path5.draw_path(win)
    path6.draw_path(win)
    path7.draw_path(win)
    path8.draw_tpath(win)
    spath1.draw_spath(win)
    spath2.draw_spath(win)
    spath3.draw_spath(win)
    spath4.draw_spath(win)
    spath5.draw_spath(win)
    opath1.draw_opath(win)
    opath2.draw_o2path(win)
    opath3.draw_o2path(win)
    opath4.draw_o1path(win)
    opath5.draw_o1path(win)
    opath6.draw_opath(win)
    mpath1.draw_mpath(win)
    mpath2.draw_mpath(win)
    mpath3.draw_mpath(win)
    mpath4.draw_mpath(win)
    mpath5.draw_mpath(win)
    mpath6.draw_mpath(win)
    mpath7.draw_mpath(win)
    mpath8.draw_mpath(win)
    mpath9.draw_mpath(win)
    mpath10.draw_mpath(win)
    mopath1.draw_mopath(win)
    mo1path1.draw_mo1path(win)
    mopath2.draw_mopath(win)
    mo1path2.draw_mo1path(win)
    mopath21.draw_mopath(win)
    mo1path21.draw_mo1path(win)
    mopath3.draw_mopath(win)
    mo1path3.draw_mo1path(win)
    mopath4.draw_mopath(win)
    mo2path1.draw_mo2path(win)
    man.draw(win)
    man1.draw1(win)
    pygame.display.update()  
#mainloop
man=player(249,410,65,90)
man1=player1(-50,-50,0,0)
text =  font.render('START', True, (5,5,5), (210,105,30))
text1 = font.render('END', True, (5,5,5), (210,105,30))  
text2 = font.render('SCORE: ' + str(score), True, (5,5,5),(255,255,0))
text3 = font.render('SCORE1: ' + str(score1), True, (5,5,5),(255,255,0))
path3=tpath(0,460,X,20,310,466,text)
path4=path(0,370,X,20)
path5=path(0,290,X,20)
path6=path(0,170,X,20)
path7=path(0,90,X,20)
path8=tpath(0,0,X,20,310,4,text1)
pathh=tpath(0,0,X,20,0,20,text2)
pathh1=tpath(0,0,X,20,520,20,text3)
spath1=spath(200,400)
spath2=spath(500,400)
spath3=spath(300,230)
spath4=spath(0,40)
spath5=spath(500,40)
opath1=spath(100,79)
opath2=spath(170,85)
opath3=spath(0,460)
opath4=spath(20,90)
opath5=spath(400,460)
opath6=spath(210,-15)
mpath1=move(300,95,speed3)
mpath2=move(0,110,speed3)
mpath3=move(400,170,speed5)
mpath4=move(-1,200,speed5)
mpath5=move(-3,290,speed6)
mpath6=move(100,320,speed6)
mpath7=move(100,370,speed3)
mpath8=move(400,10,speed3)
mpath9=move(100,10,speed3)
mpath10=move(10,10,speed3)
mopath1=move(0,165,speed3)
mo1path1=move(100,171,speed3)
mopath2=move(100,286,speed3)
mo1path2=move(0,290,speed3)
mopath21=move(200,286,speed3)
mo1path21=move(600,286,speed3)
mopath3=move(0,365,speed3)
mo1path3=move(100,369,speed3)
mopath4=move(400,365,speed3)
mo2path1=move(500,460,speed3)
run = True
while run:
    clock.tick(27)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    keys = pygame.key.get_pressed()
    if man.y>9:
        if keys[pygame.K_LEFT] and man.x > man.vel: 
            man.x -= man.vel
            man.left = True
            man.right = False

        elif keys[pygame.K_RIGHT] and man.x < 500 - man.vel - man.width:  
            man.x += man.vel
            man.left = False
            man.right = True
        elif keys[pygame.K_UP] and man.y > man.vel:
            man.y -= man.vel
            if man.y == 10:
                print(success1)
                man=player(-100,-100,0,0)
                man1=player1(249,15,65,90)
            man.up=True
            man.down=False

        elif keys[pygame.K_DOWN] and man.y < 500 - man.height - man.vel:
            man.y += man.vel
            man.up=False
            man.down=True
        
        else: 
            man.left = False
            man.right = False
            man.walkCount =0
    elif man1.y<410:
        if keys[pygame.K_l] and man1.x > man1.vel: 
            man1.x -= man1.vel
            man1.l = True
            man1.r = False
        elif keys[pygame.K_r] and man1.x < 500 - man1.vel - man1.width:  
            man1.x += man1.vel
            man1.l = False
            man1.r = True
        elif keys[pygame.K_u] and man1.y > man1.vel:
            man1.y -= man1.vel
            man1.u=True
            man1.d=False

        elif keys[pygame.K_d] and man1.y < 500 - man1.height - man1.vel:
            man1.y += man.vel
            if man1.y == 405:
                print(success2)
                man1=player1(-100,-100,0,0)
                man=player(249,410,65,90)
                man1.u=False
                man1.d=True
        else: 
            man1.l = False
            man1.r = False
            man1.walkCount =0
    #redrawGameWindow() 
    if spath1.x ==200 and spath1.y==400:
        if man.x == spath1.x or man.y==spath1.y:
            print("Hit")
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    else:
        score+=5
    if spath1.x ==200 and spath1.y==400:
        if man1.x == spath1.x or man1.y==spath1.y:
            print("Hit")
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    else:
        score1+=5

    """ if (spath1.x>=190 and spath1.x <=210) and (spath1.y>=390 and spath1.y<=410):
        if man.x==spath1.x and man.y==spath1.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    if (spath2.x >= 490 and spath2.x<=500) and (spath2.y>=390 and spath2.y <=410):
        if man.x==spath2.x and man.y==spath2.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    if (spath3.x >= 290 and spath3.x <=310 ) and (spath3.y>=220 and spath3.y<=240):
        if man.x==spath3.x and man.y==spath3.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    if (spath4.x >= -10 and spath4.x <=10) and (spath4.y >=30 and spath4.y<=50):
        if man.x==spath4.x and man.y==spath4.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    if (spath5.x >= 490  and spath5.x <=510) and (spath5.y>=30 and spath5.y<=50):
        if man.x==spath5.x and man.y==spath5.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    if (opath1.x >= 90 and opath1.x <=110) and (opath1.y>=69 and opath1.y<=89):
        if man.x==opath1.x and man.y==opath1.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    if (opath2.x >= 160 and opath2.x <=180) and (opath2.y>=75 and opath2.y<=95):
        if man.x==opath2.x and man.y==opath2.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    if (opath3.x >= -10 and opath3.x <=10) and (opath3.y>=450 and opath3.y <=470):
        if man.x==opath3.x and man.y==opath3.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    if (opath4.x >= 10 and opath4.x <=30) and (opath4.y>=80 and opath4.y<=100):
        if man.x==opath4.x and man.y==opath4.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    if (opath5.x >= 390 and opath5.x<=410) and (opath5.y>=450 and opath5.y <=470):
        if man.x==opath5.x and man.y==opath5.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)
    if (opath6.x >= 200 and opath6.x <=220) and (opath6.y>=-25 and opath6.y<=-5):
        if man.x==opath6.x and man.y==opath6.y:
            print(failure1)
            print(failure2)
            print("\t")
            man=player(-100,-100,0,0)
            man1=player1(249,15,65,90)"""
    """if (spath1.x>=190 and spath1.x <=210) and (spath1.y>=390 and spath1.y<=410):
        if man1.x==spath1.x and man1.y==spath1.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    if (spath2.x >= 490 and spath2.x<=500) and (spath2.y>=390 and spath2.y <=410):
        if man1.x==spath2.x and man1.y==spath2.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    if (spath3.x >= 290 and spath3.x <=310 ) and (spath3.y>=220 and spath3.y<=240):
        if man1.x==spath3.x and man1.y==spath3.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    if (spath4.x >= -10 and spath4.x <=10) and (spath4.y >=30 and spath4.y<=50):
        if man1.x==spath4.x and man1.y==spath4.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    if (spath5.x >= 490  and spath5.x <=510) and (spath5.y>=30 and spath5.y<=50):
        if man1.x==spath5.x and man1.y==spath5.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    if (opath1.x >= 90 and opath1.x <=110) and (opath1.y>=69 and opath1.y<=89):
        if man1.x==opath1.x and man1.y==opath1.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    if (opath2.x >= 160 and opath2.x <=180) and (opath2.y>=75 and opath2.y<=95):
        if man1.x==opath2.x and man1.y==opath2.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    if (opath3.x >= -10 and opath3.x <=10) and (opath3.y>=450 and opath3.y <=470):
        if man1.x==opath3.x and man1.y==opath3.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    if (opath4.x >= 10 and opath4.x <=30) and (opath4.y>=80 and opath4.y<=100):
        if man1.x==opath4.x and man1.y==opath4.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    if (opath5.x >= 390 and opath5.x<=410) and (opath5.y>=450 and opath5.y <=470):
        if man1.x==opath5.x and man1.y==opath5.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)
    if (opath6.x >= 200 and opath6.x <=220) and (opath6.y>=-25 and opath6.y<=-5):
        if man1.x==opath6.x and man1.y==opath6.y:
            print(failure1)
            print(failure2)
            print("\t")
            man1=player1(-100,-100,0,0)
            man=player(249,15,65,90)"""
    redrawGameWindow() 
pygame.quit()

