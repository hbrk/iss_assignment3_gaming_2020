...readme for Crossy River game...
For player,
    for going up press up arrow key,
	for going down press down arrow key,
	for going right press right arrow key,
	for going left press left arrow key.
For player1,
    	for going up press 'u' key,
	for going down press 'd' key,
	for going right press 'r' key,
	for going left press 'l' key.
The functioning of game:
	Playing arena consists of a river with some partitions in it. There are 2 players in the game, one at Top and another at Bottom (2 sides of the river bank). A player is safe when it is standing on a partition/slab. There are two kinds of obstacles, moving and fixed. The player starts from the ‘START’ partition and must reach the ‘END’ position for player 1 and the ‘END’ becomes ‘START’, ‘START’ becomes ‘END’ for Player 2. The moving obstacles move from left to right. The player can move up, down, left and right. Player dies once he/she touches any obstacle. As player crosses moving obstacle successfully, accrues 10 points and for crossing fixed obstacles 5 points. Only one player is playing at a given point of time. Your aim is to make players reach the other end of the river. The player wins the game based on
		a) Time taken to cross.
		b) Points accrued while crossing obstacles.
	At the end of the game (after both the players either die or reach other ‘END’), the players are respawned
	to their starting position. If the player wins a round, the speed of moving objects increases in the next
	round for that player.
	When the player hit the fixed or moving obstacle,it dies and prints the message "Hit WITH AN OBSTACLE" and "GAME OVER FOR PLAYER"(Corresponding player).
	When the player reaches their end paths ,the corresponding success message will be printed.
The config file written in JSON consists of the messages is imported in main.py file.

